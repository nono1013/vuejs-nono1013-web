import Vue from 'vue'
import App from './App.vue'
import Archive from './Archive.vue'
require('vuetify/src/stylus/app.styl')

const routes = {
  '/': App,
  '/archive': Archive
}

new Vue({
  el: '#app',
  data: {
    currentRoute: window.location.pathname
  },
  computed: {
    ViewComponent () {
      return routes[this.currentRoute] || App
    }
  },
  render (h) { return h(this.ViewComponent) }
})
